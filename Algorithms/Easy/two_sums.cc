//Initial submissions

class Solution {
public:
  vector<int> twoSum(vector<int>& nums, int target) {
    vector<int> indice;
    for (int i = 0; i<nums.size()-1; i++)
    {
        for (int j = i+1; j<nums.size(); j++)
        {
          if (nums[i] + nums[j] == target)
          {
            indice.push_back(i);
            indice.push_back(j);
            return indice;
          }
        }
    } return indice;
  }
};

// Second submission

class Solution {
public:
  vector<int> twoSum(vector<int>& nums, int target) {
    unordered_map<int, int> imap;
    
    for (int i = 0;; ++i) {
      auto it = imap.find(target - nums[i]);

      if (it != imap.end()) 
        return vector<int> {i, it->second};

      imap[nums[i]] = i;
    }
  }
};